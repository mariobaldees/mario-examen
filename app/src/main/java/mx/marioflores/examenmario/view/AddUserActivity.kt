package mx.marioflores.examenmario.view

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_add_user.*
import mx.marioflores.examenmario.R
import mx.marioflores.myexamenmario.controller.Controller
import mx.marioflores.myexamenmario.model.*


class AddUserActivity : AppCompatActivity() {
    var name:String = ""
    var area:String = ""
    var numeronomina:Int = 0
    lateinit var _controller : Controller

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
        _controller = Controller()
        btnadd.setOnClickListener{
            name = nombreuser.text.toString()
            area = areauser.text.toString()
            var nom = numeronominauser.text.toString()

            if(name != "" && area != "" && nom != "") {
                numeronomina = Integer.parseInt(nom)
                _controller.addUserToServer(name,area,numeronomina)
            }else{
                showErrorDialog()
            }
        }
        _controller.aggUserModel().observe(this, Observer<UsersModel> {
                userModel: UsersModel?->
            if(userModel!=null){
                cerrarActivity(userModel)
            }
        })
        _controller.getMessageAddUser().observe(this, Observer<String> {
                userModel: String?->
            if(userModel!=null){
                showErrorInsert(userModel)
            }
        })



    }
    fun showErrorInsert(message:String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ERROR")
        builder.setMessage(message)

        builder.setPositiveButton("OK") { dialog, which ->
            dialog.dismiss()
        }

        builder.show()
    }
    fun showErrorDialog(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ADVERTENCIA")
        builder.setMessage("HAY ALGUNOS CAMPOS VACÍOS")

        builder.setPositiveButton("OK") { dialog, which ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun cerrarActivity(mUsersModel:UsersModel?) {
        val b = Bundle()
        if(mUsersModel != null) {
            b.putString("name", mUsersModel.name)
            b.putString("area", mUsersModel.area)
            b.putInt("nomina", mUsersModel!!.numeroNomina)
            b.putInt("id", mUsersModel!!.idUser)
        }
        val intent = getIntent()
        intent.putExtras(b)
        setResult(Activity.RESULT_OK,intent)
        super.finish()

    }
}

