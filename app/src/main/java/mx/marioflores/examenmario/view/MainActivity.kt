package mx.marioflores.examenmario.view

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import mx.marioflores.examenmario.R
import mx.marioflores.myexamenmario.controller.Controller
import mx.marioflores.myexamenmario.model.UsersAdapter
import mx.marioflores.myexamenmario.model.UsersModel

class MainActivity : AppCompatActivity() {
    private var mUsersModel: List<UsersModel> = ArrayList()
    lateinit var  recyclerAdapter: UsersAdapter
    lateinit var _controller : Controller

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        _controller = Controller()

        fab.setOnClickListener{
            val intent = Intent(this@MainActivity, AddUserActivity::class.java)
            //intent.putExtra("key", "Kotlin")
            startActivityForResult(intent,100)
        }

        _controller.getUserModel().observe(this, Observer<List<UsersModel>> {
                userModel: List<UsersModel>?->
            if(userModel!=null){
                mUsersModel = userModel
                recyclerAdapter = UsersAdapter(mUsersModel as MutableList<UsersModel>,
                    R.layout.item_list
                )
                my_recycler_view.layoutManager = LinearLayoutManager(applicationContext)
                my_recycler_view.adapter = recyclerAdapter
                recyclerAdapter.notifyDataSetChanged()
            }
        })
        _controller.getMessageGetUserModel().observe(this, Observer<String> {
                userModel: String?->
            if(userModel!=null){
                showErrorDialog(userModel)
            }
        })

        _controller.getUsers()



    }
    fun showErrorDialog(message:String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ERROR")
        builder.setMessage(message)

        builder.setPositiveButton("OK") { dialog, which ->
            dialog.dismiss()
            finish()
        }

        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == 100 && resultCode == Activity.RESULT_OK){

            var id:Int = data!!.getIntExtra("id",0)
            var nomina = data!!.getIntExtra("nomina",0)
            var name = data!!.getStringExtra("name")
            var area = data!!.getStringExtra("area")

            var usersModel = UsersModel(id,nomina,name,area)

            recyclerAdapter.insert(1,usersModel)
           // recyclerAdapter.notifyDataSetChanged()


        }
    }


}