package mx.marioflores.myexamenmario.model

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.*

interface UsersInterface {
    @Headers("Content-Type: application/json")
    @GET("examen/alpura/web-service/examen/users")
    //fun getUsers():Call<List<UsersModel>>
    fun getUsers():Call<List<UsersModel>>
    //fun listRepos(): Callback<List<UsersModel?>>

    @Headers("Content-Type: application/json")
    @POST("examen/alpura/web-service/examen/users")
    fun addUser(@Body addUserModel: AddUserModel):Call<UsersModel>
}