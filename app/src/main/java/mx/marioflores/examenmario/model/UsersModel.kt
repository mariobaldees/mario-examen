package mx.marioflores.myexamenmario.model

import com.google.gson.annotations.SerializedName

class UsersModel {
    @SerializedName("idUser")
    var idUser: Int = 0

    @SerializedName("numeroNomina")
    var numeroNomina:Int = 0

    @SerializedName("name")
    var name:String = ""

    @SerializedName("area")
    var area:String = ""

    constructor(idUser: Int,numeroNomina: Int,name: String,area: String){
        this.idUser = idUser
        this.numeroNomina = numeroNomina
        this.name = name
        this.area = area
    }

}
class AddUserModel{
    @SerializedName("numeroNomina")
    var numeroNomina:Int = 0

    @SerializedName("name")
    var name:String = ""

    @SerializedName("area")
    var area:String = ""

    constructor(numeroNomina: Int,name: String,area: String){

        this.numeroNomina = numeroNomina
        this.name = name
        this.area = area
    }
}


