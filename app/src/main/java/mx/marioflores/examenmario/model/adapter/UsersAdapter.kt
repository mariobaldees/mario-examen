package mx.marioflores.myexamenmario.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.marioflores.examenmario.R
import java.util.*


class UsersAdapter(private var userList : MutableList<UsersModel>,private val itemUser: Int):RecyclerView.Adapter<UsersAdapter.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersAdapter.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(itemUser,parent,false)
        //this.list = userList
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        if(userList != null){
            return userList.size
        }
        return 0
    }
    fun insert(position: Int, data: UsersModel) {
        userList.add(data)

        notifyItemInserted(position)
        notifyItemChanged(position)

    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       holder.idUser.setText(userList.get(position).idUser.toString())
       holder.numeroNomina.setText(userList.get(position).numeroNomina.toString())
       holder.name.setText(userList.get(position).name)
       holder.area.setText(userList.get(position).area)
    }

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var idUser : TextView = itemView.findViewById(R.id.idUser)
        var numeroNomina : TextView = itemView.findViewById(R.id.numNomina)
        var name : TextView = itemView.findViewById(R.id.name)
        var area : TextView = itemView.findViewById(R.id.area)
    }

}