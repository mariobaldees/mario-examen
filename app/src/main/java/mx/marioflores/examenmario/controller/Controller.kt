package mx.marioflores.myexamenmario.controller

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import mx.marioflores.examenmario.R
import mx.marioflores.myexamenmario.model.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Controller{

    var _userModel: MutableLiveData<List<UsersModel>> = MutableLiveData()
    var _aggUserModel: MutableLiveData<UsersModel> = MutableLiveData()
    var _messagegetUser: MutableLiveData<String> = MutableLiveData()
    var _messageAddUser: MutableLiveData<String> = MutableLiveData()

    constructor (){
        _userModel.postValue(null)
        _aggUserModel.postValue(null)
        _messagegetUser.postValue(null)
        _messageAddUser.postValue(null)
    }

    fun getMessageGetUserModel(): MutableLiveData<String> {
        return _messagegetUser
    }
    fun getMessageAddUser(): MutableLiveData<String> {
        return _messageAddUser
    }
    fun getUserModel(): MutableLiveData<List<UsersModel>> {
        return _userModel
    }
    fun aggUserModel():MutableLiveData<UsersModel>{
        return _aggUserModel
    }

    fun getUsers(){
        val retrofit = ServiceBuilder.getRetrofitInstance()?.create(UsersInterface::class.java)

        if(retrofit != null){
            var call: Call<List<UsersModel>> = retrofit?.getUsers()
            if (call != null) {
                call.enqueue(
                    object : Callback<List<UsersModel>> {


                        override fun onFailure(
                            call: Call<List<UsersModel>>,
                            t: Throwable
                        ) {
                            _messagegetUser.postValue(t.message);
                        }

                        override fun onResponse(
                            call: Call<List<UsersModel>>,
                            response: Response<List<UsersModel>>
                        ) {
                            val users = response.body()
                            if (users != null) {
                                _userModel.postValue(users)

                            }
                        }

                    }
                )
            }
        }
    }
    fun addUserToServer(name: String,area:String,numeronomina:Int) {
        val retrofit = ServiceBuilder.getRetrofitInstance()?.create(UsersInterface::class.java)
        var mAddUserModel = AddUserModel(numeronomina, name, area)
        if (retrofit != null) {
            retrofit.addUser(mAddUserModel).enqueue(
                object : Callback<UsersModel> {
                    override fun onFailure(call: Call<UsersModel>, t: Throwable) {
                        _messageAddUser.postValue(t.message)
                    }

                    override fun onResponse(
                        call: Call<UsersModel>,
                        response: Response<UsersModel>
                    ) {
                        _aggUserModel.postValue(response.body())

                    }

                }
            )
        }
    }


    }


